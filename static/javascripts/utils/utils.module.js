(function () {
  'use strict';

  angular
    .module('cura.utils', [
      'cura.utils.services'
    ]);

  angular
    .module('cura.utils.services', []);
})();