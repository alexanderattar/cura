/**
* Creation
* @namespace cura.creations.directives
*/
(function () {
  'use strict';

  angular
    .module('cura.creations.directives')
    .directive('creation', creation);

  /**
  * @namespace Post
  */
  function creation() {
    /**
    * @name directive
    * @desc The directive to be returned
    * @memberOf cura.creations.directives.Post
    */
    var directive = {
      restrict: 'E',
      scope: {
        creation: '='
      },
      templateUrl: '/static/templates/creations/creation.html'
    };

    return directive;
  }
})();