(function () {
  'use strict';

  angular
    .module('cura.creations', [
      'cura.creations.controllers',
      'cura.creations.directives',
      'cura.creations.services'
    ]);

  angular
    .module('cura.creations.controllers', []);

  angular
    .module('cura.creations.directives', ['ngDialog']);

  angular
    .module('cura.creations.services', []);
})();