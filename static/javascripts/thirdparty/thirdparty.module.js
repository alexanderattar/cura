(function () {
  'use strict';

  angular
    .module('cura.thirdparty', [
      'flow',
      'flow.config'
    ])

  angular
    .module('flow.config', []);

})();