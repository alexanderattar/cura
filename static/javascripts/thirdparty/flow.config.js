(function () {
  'use strict';

  angular
    .module('flow.config')
    .config(['flowFactoryProvider', function (flowFactoryProvider) {
      flowFactoryProvider.defaults = {
        target: '/api/v1/creations/1/image/',
        permanentErrors: [404, 500, 501],
        uploadMethod: 'POST',
        headers: {''}  // TODO - Need logic to add CSRF Token for Flow requests
      };
      // You can also set default events:
      flowFactoryProvider.on('catchAll', function (event) {
        console.log('catchAll', arguments);
      });

    }]);

})();