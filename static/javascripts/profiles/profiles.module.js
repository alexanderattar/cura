(function () {
  'use strict';

  angular
    .module('cura.profiles', [
      'cura.profiles.controllers',
      'cura.profiles.services'
    ]);

  angular
    .module('cura.profiles.controllers', []);

  angular
    .module('cura.profiles.services', []);
})();