/**
* ProfileController
* @namespace cura.profiles.controllers
*/
(function () {
  'use strict';

  angular
    .module('cura.profiles.controllers')
    .controller('ProfileController', ProfileController);

  ProfileController.$inject = ['$location', '$routeParams', 'Creations', 'Profile', 'Snackbar'];

  /**
  * @namespace ProfileController
  */
  function ProfileController($location, $routeParams, Creations, Profile, Snackbar) {
    var vm = this;

    vm.profile = undefined;
    vm.creations = [];

    activate();

    /**
    * @name activate
    * @desc Actions to be performed when this controller is instantiated
    * @memberOf cura.profiles.controllers.ProfileController
    */
    function activate() {
      var username = $routeParams.username;

      Profile.get(username).then(profileSuccessFn, profileErrorFn);
      Creations.get(username).then(creationsSuccessFn, creationsErrorFn);

      /**
      * @name profileSuccessProfile
      * @desc Update `profile` on viewmodel
      */
      function profileSuccessFn(data, status, headers, config) {
        vm.profile = data.data;
      }


      /**
      * @name profileErrorFn
      * @desc Redirect to index and show error Snackbar
      */
      function profileErrorFn(data, status, headers, config) {
        $location.url('/');
        Snackbar.error('That user does not exist.');
      }


      /**
        * @name creationsSucessFn
        * @desc Update `creations` on viewmodel
        */
      function creationsSuccessFn(data, status, headers, config) {
        vm.creations = data.data;
      }


      /**
        * @name creationsErrorFn
        * @desc Show error snackbar
        */
      function creationsErrorFn(data, status, headers, config) {
        Snackbar.error(data.data.error);
      }
    }
  }
})();