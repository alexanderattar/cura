/**
* IndexController
* @namespace cura.layout.controllers
*/
(function () {
  'use strict';

  angular
    .module('cura.layout.controllers')
    .controller('IndexController', IndexController);

  IndexController.$inject = ['$scope', 'Authentication', 'Creations', 'Snackbar'];

  /**
  * @namespace IndexController
  */
  function IndexController($scope, Authentication, Creations, Snackbar) {
    var vm = this;

    vm.isAuthenticated = Authentication.isAuthenticated();
    vm.creations = [];

    activate();

    /**
    * @name activate
    * @desc Actions to be performed when this controller is instantiated
    * @memberOf cura.layout.controllers.IndexController
    */
    function activate() {
      Creations.all().then(creationsSuccessFn, creationsErrorFn);

      $scope.$on('creation.created', function (event, creation) {
        vm.creations.unshift(creation);
      });

      $scope.$on('creation.created.error', function () {
        vm.creations.shift();
      });


      /**
      * @name creationsSuccessFn
      * @desc Update creations array on view
      */
      function creationsSuccessFn(data, status, headers, config) {
        vm.creations = data.data;
      }


      /**
      * @name creationsErrorFn
      * @desc Show snackbar with error
      */
      function creationsErrorFn(data, status, headers, config) {
        Snackbar.error(data.error);
      }
    }
  }
})();