(function () {
  'use strict';

  angular
    .module('cura.layout', [
      'cura.layout.controllers'
    ]);

  angular
    .module('cura.layout.controllers', []);
})();