(function () {
  'use strict';

  angular
    .module('cura.authentication', [
      'cura.authentication.controllers',
      'cura.authentication.services'
    ]);

  angular
    .module('cura.authentication.controllers', []);

  angular
    .module('cura.authentication.services', ['ngCookies']);
})();