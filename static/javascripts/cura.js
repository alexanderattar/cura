(function () {
  'use strict';

  angular
    .module('cura', [
      'cura.config',
      'cura.routes',
      'cura.authentication',
      'cura.layout',
      'cura.creations',
      'cura.utils',
      'cura.profiles',

      'cura.thirdparty'
    ])

  angular
    .module('cura.config', []);

  angular
    .module('cura.routes', ['ngRoute']);

  angular
    .module('cura')
    .run(run);

  run.$inject = ['$http'];

  /**
  * @name run
  * @desc Update xsrf $http headers to align with Django's defaults
  */
  function run($http) {
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
  }

})();