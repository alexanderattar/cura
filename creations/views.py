from rest_framework import status
from rest_framework import permissions, viewsets
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.decorators import detail_route, parser_classes
from rest_framework.mixins import RetrieveModelMixin, UpdateModelMixin

from creations.models import Creation
from creations.permissions import IsCreator
from creations.serializers import CreationSerializer


# class CreationViewSet(viewsets.ModelViewSet):
#     queryset = Creation.objects.order_by('-created')
#     serializer_class = CreationSerializer

#     def get_permissions(self):
#         if self.request.method in permissions.SAFE_METHODS:
#             return (permissions.AllowAny(),)
#         return (permissions.IsAuthenticated(), IsCreator(),)

#     def perform_create(self, serializer):
#         instance = serializer.save(creator=self.request.user)

#         return super(CreationViewSet, self).perform_create(serializer)


class AccountCreationsViewSet(viewsets.ViewSet):
    queryset = Creation.objects.select_related('creator').all()
    serializer_class = CreationSerializer

    def list(self, request, account_username=None):
        queryset = self.queryset.filter(creator__username=account_username)
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)


class CreationViewSet(viewsets.ModelViewSet):
    queryset = Creation.objects.order_by('-created')
    serializer_class = CreationSerializer
    permission_classes = (IsCreator,)

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)

    # def get_permissions(self):
    #     if self.request.method in permissions.SAFE_METHODS:
    #         return (permissions.AllowAny(),)
    #     return (permissions.IsAuthenticated(), IsCreator(),)

    # def perform_create(self, serializer):
    #     instance = serializer.save(creator=self.request.user)

    #     return super(CreationViewSet, self).perform_create(serializer)

    # @detail_route(methods=['POST'], permission_classes=[IsCreator])
    # @parser_classes((FormParser, MultiPartParser,))
    # def image(self, request, *args, **kwargs):
    #     if 'upload' in request.data:
    #         instance = self.get_object()
    #         instance.image.delete()

    #         upload = request.data['upload']

    #         instance.image.save(upload.name, upload)

    #         return Response(status=status.HTTP_201_CREATED, headers={'Location': instance.image.url})
    #     else:
    #         return Response(status=status.HTTP_400_BAD_REQUEST)

    # TODO - Get rid of these unprotected versions of these methods.. just for testing now
    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        # return (permissions.IsAuthenticated(), IsCreator(),)
        return (permissions.AllowAny(),)

    def perform_create(self, serializer):
        instance = serializer.save(creator=self.request.user)

        return super(CreationViewSet, self).perform_create(serializer)

    @detail_route(methods=['POST'])
    @parser_classes((FormParser, MultiPartParser,))
    def image(self, request, *args, **kwargs):
        print('++++++++')
        print(request.data)
        # print('++++++++')
        # if 'upload' in request.data:
        #     instance = self.get_object()
        #     instance.image.delete()
        #     upload = request.data['upload']
        #     instance.image.save(upload.name, upload)

        #     return Response(status=status.HTTP_201_CREATED, headers={'Location': instance.image.url})
        # else:
        #     return Response(status=status.HTTP_400_BAD_REQUEST)
