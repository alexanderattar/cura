from django.db import models

from authentication.models import Account


class Creation(models.Model):
    creator = models.ForeignKey(Account)
    description = models.TextField()
    image = models.ImageField(upload_to='photos', max_length=256, null=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '{0}'.format(self.description)
