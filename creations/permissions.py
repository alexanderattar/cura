from rest_framework import permissions


class IsCreator(permissions.BasePermission):
    def has_object_permission(self, request, view, creation):
        if request.user:
            return creation.creator == request.user
        return False
